package tw.edu.ntu.esoe.wetu.test;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import tw.edu.ntu.esoe.wetu.web.SslUtils;
import tw.edu.ntu.esoe.wetu.web.data.DataUtils;
import tw.edu.ntu.esoe.wetu.web.data.JsonUtils;
import tw.edu.ntu.esoe.wetu.web.data.geocoding.GeocodingResult;
import tw.edu.ntu.esoe.wetu.web.data.geocoding.NominatimRequest;
import tw.edu.ntu.esoe.wetu.web.data.weather.ForecastWeatherData;
import tw.edu.ntu.esoe.wetu.web.data.weather.OpenWeatherMapRequest;

public class TestApiRequest {

	public static void main(String[] args) {
		try {
			SslUtils.setDefaultSSLSocketFactory();
		}
		catch(IOException | GeneralSecurityException e) {
			throw new RuntimeException("Unable to set SSL socket factory.", e);
		}
		try {
			ForecastWeatherData fwd = OpenWeatherMapRequest.getForecast(25.016, 121.536, "zh_tw");
			DataUtils.writeData("testOneCall.json", JsonUtils.GSON.toJsonTree(fwd));
			List<GeocodingResult> results = NominatimRequest.searchLocation("學府里", "zh-Hant");
			DataUtils.writeData("testNominatim.json", JsonUtils.GSON.toJsonTree(results));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
