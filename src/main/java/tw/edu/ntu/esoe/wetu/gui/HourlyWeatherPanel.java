package tw.edu.ntu.esoe.wetu.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import tw.edu.ntu.esoe.wetu.userdata.Settings;
import tw.edu.ntu.esoe.wetu.web.data.weather.HourlyWeather;

public class HourlyWeatherPanel extends JPanel {

	public static final int PANEL_HEIGHT = 120;

	public static final Settings SETTINGS = WetuWindow.SETTINGS;

	public static final Color CELESTE = new Color(0xB2FFFF);

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("MM/dd (E)");
	public static final DateTimeFormatter ZONED_TIME = DateTimeFormatter.ofPattern("HH:mm O");

	private final JLabel dateLabel;
	private final JLabel timeLabel;
	private final JLabel weatherCatLabel;
	private final JLabel weatherIconLabel;
	private final JLabel weatherDescLabel;
	private final JLabel tempCatLabel;
	private final JLabel tempLabel;
	private final JLabel apparentCatLabel;
	private final JLabel apparentLabel;
	private final JLabel popCatLabel;
	private final JLabel popLabel;
	private final JLabel humidityCatLabel;
	private final JLabel humidityLabel;
	private final JLabel windSpeedCatLabel;
	private final JLabel windSpeedLabel;
	private final JLabel windGustCatLabel;
	private final JLabel windGustLabel;
	private final JLabel windDirCatLabel;
	private final JLabel windDirLabel;
	private final JLabel uviCatLabel;
	private final JLabel uviLabel;

	private final HourlyWeather hourlyWeather;
	private final ZoneId zoneId;

	HourlyWeatherPanel(HourlyWeather hourlyWeather, ZoneId zoneId) {
		super();
		this.hourlyWeather = hourlyWeather;
		this.zoneId = zoneId;

		setLayout(null);
		setBackground(CELESTE);
		setBorder(BorderFactory.createEtchedBorder());
		setPreferredSize(new Dimension(getWidth(), PANEL_HEIGHT));

		dateLabel = new JLabel();
		dateLabel.setFont(getFont());
		dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(dateLabel);

		timeLabel = new JLabel();
		timeLabel.setFont(getFont());
		timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(timeLabel);

		weatherCatLabel = new JLabel("天氣狀況");
		weatherCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(weatherCatLabel);

		weatherIconLabel = new JLabel();
		weatherIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
		weatherIconLabel.setVerticalAlignment(SwingConstants.CENTER);
		add(weatherIconLabel);

		weatherDescLabel = new JLabel();
		weatherDescLabel.setFont(getFont());
		weatherDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(weatherDescLabel);

		tempCatLabel = new JLabel("溫度");
		tempCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(tempCatLabel);

		tempLabel = new JLabel();
		tempLabel.setFont(getFont().deriveFont(24F));
		tempLabel.setHorizontalAlignment(SwingConstants.CENTER);
		tempLabel.setVerticalAlignment(SwingConstants.CENTER);
		add(tempLabel);

		apparentCatLabel = new JLabel("體感溫度");
		apparentCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(apparentCatLabel);

		apparentLabel = new JLabel();
		apparentLabel.setFont(getFont());
		apparentLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(apparentLabel);

		popCatLabel = new JLabel("降雨機率");
		popCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(popCatLabel);

		popLabel = new JLabel();
		popLabel.setFont(getFont());
		popLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(popLabel);

		humidityCatLabel = new JLabel("相對溼度");
		humidityCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(humidityCatLabel);

		humidityLabel = new JLabel();
		humidityLabel.setFont(getFont());
		humidityLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(humidityLabel);

		windSpeedCatLabel = new JLabel("風力");
		windSpeedCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windSpeedCatLabel);

		windSpeedLabel = new JLabel();
		windSpeedLabel.setFont(getFont());
		windSpeedLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windSpeedLabel);

		windGustCatLabel = new JLabel("陣風");
		windGustCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windGustCatLabel);

		windGustLabel = new JLabel();
		windGustLabel.setFont(getFont());
		windGustLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windGustLabel);

		windDirCatLabel = new JLabel("風向");
		windDirCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windDirCatLabel);

		windDirLabel = new JLabel();
		windDirLabel.setFont(getFont());
		windDirLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windDirLabel);

		uviCatLabel = new JLabel("紫外線");
		uviCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(uviCatLabel);

		uviLabel = new JLabel();
		uviLabel.setFont(getFont());
		uviLabel.setOpaque(true);
		uviLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(uviLabel);

		setData();
	}

	@Override
	public void doLayout() {
		int s = getWidth();
		int s1 = s*1/9;
		int s2 = s*4/15;
		int s3 = s*19/45;
		int s4 = s*26/45;
		int s5 = s*11/15;
		int s6 = s*8/9;
		dateLabel.setBounds(s1-40, 40, 80, 20);
		timeLabel.setBounds(s1-50, 60, 100, 20);
		weatherCatLabel.setBounds(s2-40, 15, 80, 20);
		weatherIconLabel.setBounds(s2-40, 35, 80, 50);
		weatherDescLabel.setBounds(s2-40, 85, 80, 20);
		tempCatLabel.setBounds(s3-60, 15, 120, 20);
		tempLabel.setBounds(s3-60, 35, 120, 30);
		apparentCatLabel.setBounds(s3-60, 65, 120, 20);
		apparentLabel.setBounds(s3-60, 85, 120, 20);
		popCatLabel.setBounds(s4-40, 20, 80, 20);
		popLabel.setBounds(s4-40, 40, 80, 20);
		humidityCatLabel.setBounds(s4-40, 60, 80, 20);
		humidityLabel.setBounds(s4-40, 80, 80, 20);
		windSpeedCatLabel.setBounds(s5-50, 20, 50, 20);
		windSpeedLabel.setBounds(s5-50, 40, 50, 20);
		windGustCatLabel.setBounds(s5, 20, 50, 20);
		windGustLabel.setBounds(s5, 40, 50, 20);
		windDirCatLabel.setBounds(s5-40, 60, 80, 20);
		windDirLabel.setBounds(s5-40, 80, 80, 20);
		uviCatLabel.setBounds(s6-60, 40, 120, 20);
		uviLabel.setBounds(s6-15, 60, 30, 20);
	}

	void setData() {
		ZonedDateTime weatherTime = Utils.ofEpochSecond(hourlyWeather.getDt(), zoneId);
		dateLabel.setText(DATE.format(weatherTime));
		timeLabel.setText(ZONED_TIME.format(weatherTime));
		Icon icon = Icons.getIcon(hourlyWeather.getClouds(), hourlyWeather.getWeather(), 50, 50);
		weatherIconLabel.setIcon(icon);
		weatherDescLabel.setText(Utils.getWeatherDesc(hourlyWeather.getWeather()));
		tempLabel.setText(String.format("%.1f", hourlyWeather.getTemp())+"˚C");
		apparentLabel.setText(String.format("%.1f", hourlyWeather.getFeelsLike())+"˚C");
		popLabel.setText(Math.round(hourlyWeather.getPop()*100)+"%");
		humidityLabel.setText(hourlyWeather.getHumidity()+"%");
		windSpeedLabel.setText(Utils.getWindDesc(hourlyWeather.getWindSpeed()));
		windGustLabel.setText(Utils.getWindDesc(hourlyWeather.getWindGust()));
		windDirLabel.setText(Utils.getDirectionDesc(hourlyWeather.getWindDeg()));
		double uvi = hourlyWeather.getUvi();
		if(uvi < 3) {
			uviLabel.setBackground(Color.GREEN);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 6) {
			uviLabel.setBackground(Color.YELLOW);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 8) {
			uviLabel.setBackground(Color.ORANGE);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 11) {
			uviLabel.setBackground(Color.RED);
			uviLabel.setForeground(Color.WHITE);
		}
		else {
			uviLabel.setBackground(Color.MAGENTA);
			uviLabel.setForeground(Color.WHITE);
		}
		uviLabel.setText(String.format("%.1f", uvi));
	}
}
