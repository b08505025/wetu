package tw.edu.ntu.esoe.wetu.gui;

import java.awt.Color;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import tw.edu.ntu.esoe.wetu.userdata.Settings;
import tw.edu.ntu.esoe.wetu.web.data.weather.CurrentWeather;

public class CurrentWeatherPanel extends JPanel {

	public static final Settings SETTINGS = WetuWindow.SETTINGS;

	public static final Color CELESTE = new Color(0xB2FFFF);

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("MM/dd (E)");
	public static final DateTimeFormatter ZONED_TIME = DateTimeFormatter.ofPattern("HH:mm O");
	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm");

	private final JLabel dateLabel;
	private final JLabel timeLabel;
	private final JLabel weatherCatLabel;
	private final JLabel weatherIconLabel;
	private final JLabel weatherDescLabel;
	private final JLabel tempCatLabel;
	private final JLabel tempLabel;
	private final JLabel apparentCatLabel;
	private final JLabel apparentLabel;
	private final JLabel precipitationCatLabel;
	private final JLabel precipitationLabel;
	private final JLabel humidityCatLabel;
	private final JLabel humidityLabel;
	private final JLabel windSpeedCatLabel;
	private final JLabel windSpeedLabel;
	private final JLabel windGustCatLabel;
	private final JLabel windGustLabel;
	private final JLabel windDirCatLabel;
	private final JLabel windDirLabel;
	private final JLabel uviCatLabel;
	private final JLabel uviLabel;
	private final JLabel sunriseCatLabel;
	private final JLabel sunriseLabel;
	private final JLabel sunsetCatLabel;
	private final JLabel sunsetLabel;

	private CurrentWeather currentWeather;
	private ZoneId zoneId;

	CurrentWeatherPanel() {
		super();

		setLayout(null);
		setBackground(CELESTE);

		dateLabel = new JLabel();
		dateLabel.setFont(getFont());
		dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(dateLabel);

		timeLabel = new JLabel();
		timeLabel.setFont(getFont());
		timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(timeLabel);

		weatherCatLabel = new JLabel("天氣狀況");
		weatherCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(weatherCatLabel);

		weatherIconLabel = new JLabel();
		weatherIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
		weatherIconLabel.setVerticalAlignment(SwingConstants.CENTER);
		add(weatherIconLabel);

		weatherDescLabel = new JLabel();
		weatherDescLabel.setFont(getFont());
		weatherDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(weatherDescLabel);

		tempCatLabel = new JLabel("溫度");
		tempCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(tempCatLabel);

		tempLabel = new JLabel();
		tempLabel.setFont(getFont().deriveFont(40F));
		tempLabel.setHorizontalAlignment(SwingConstants.CENTER);
		tempLabel.setVerticalAlignment(SwingConstants.CENTER);
		add(tempLabel);

		apparentCatLabel = new JLabel("體感溫度");
		apparentCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(apparentCatLabel);

		apparentLabel = new JLabel();
		apparentLabel.setFont(getFont());
		apparentLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(apparentLabel);

		precipitationCatLabel = new JLabel("時雨量");
		precipitationCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(precipitationCatLabel);

		precipitationLabel = new JLabel();
		precipitationLabel.setFont(getFont());
		precipitationLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(precipitationLabel);

		humidityCatLabel = new JLabel("相對溼度");
		humidityCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(humidityCatLabel);

		humidityLabel = new JLabel();
		humidityLabel.setFont(getFont());
		humidityLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(humidityLabel);

		windSpeedCatLabel = new JLabel("風力");
		windSpeedCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windSpeedCatLabel);

		windSpeedLabel = new JLabel();
		windSpeedLabel.setFont(getFont());
		windSpeedLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windSpeedLabel);

		windGustCatLabel = new JLabel("陣風");
		windGustCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windGustCatLabel);

		windGustLabel = new JLabel();
		windGustLabel.setFont(getFont());
		windGustLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windGustLabel);

		windDirCatLabel = new JLabel("風向");
		windDirCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windDirCatLabel);

		windDirLabel = new JLabel();
		windDirLabel.setFont(getFont());
		windDirLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windDirLabel);

		uviCatLabel = new JLabel("紫外線");
		uviCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(uviCatLabel);

		uviLabel = new JLabel();
		uviLabel.setFont(getFont());
		uviLabel.setOpaque(true);
		uviLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(uviLabel);

		sunriseCatLabel = new JLabel("日出");
		sunriseCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(sunriseCatLabel);

		sunriseLabel = new JLabel();
		sunriseLabel.setFont(getFont());
		sunriseLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(sunriseLabel);

		sunsetCatLabel = new JLabel("日沒");
		sunsetCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(sunsetCatLabel);

		sunsetLabel = new JLabel();
		sunsetLabel.setFont(getFont());
		sunsetLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(sunsetLabel);
	}

	@Override
	public void doLayout() {
		int s = getWidth();
		int s1 = s*1/6;
		int s2 = s*1/2;
		int s3 = s*5/6;
		dateLabel.setBounds(s1-40, 5, 80, 20);
		timeLabel.setBounds(s1-50, 25, 100, 20);
		weatherCatLabel.setBounds(s1-40, 55, 80, 20);
		weatherIconLabel.setBounds(s1-40, 75, 80, 50);
		weatherDescLabel.setBounds(s1-40, 125, 80, 20);
		tempCatLabel.setBounds(s2-60, 45, 120, 20);
		tempLabel.setBounds(s2-70, 65, 140, 50);
		apparentCatLabel.setBounds(s2-60, 115, 120, 20);
		apparentLabel.setBounds(s2-60, 135, 120, 20);
		precipitationCatLabel.setBounds(s3-40, 55, 80, 20);
		precipitationLabel.setBounds(s3-40, 75, 80, 20);
		humidityCatLabel.setBounds(s3-40, 105, 80, 20);
		humidityLabel.setBounds(s3-40, 125, 80, 20);
		windSpeedCatLabel.setBounds(s1-50, 205, 50, 20);
		windSpeedLabel.setBounds(s1-50, 225, 50, 20);
		windGustCatLabel.setBounds(s1, 205, 50, 20);
		windGustLabel.setBounds(s1, 225, 50, 20);
		windDirCatLabel.setBounds(s1-40, 245, 80, 20);
		windDirLabel.setBounds(s1-40, 265, 80, 20);
		uviCatLabel.setBounds(s2-60, 225, 120, 20);
		uviLabel.setBounds(s2-15, 245, 30, 20);
		sunriseCatLabel.setBounds(s3-40, 205, 80, 20);
		sunriseLabel.setBounds(s3-40, 225, 80, 20);
		sunsetCatLabel.setBounds(s3-40, 245, 80, 20);
		sunsetLabel.setBounds(s3-40, 265, 80, 20);
	}

	void setData(CurrentWeather currentWeather, ZoneId zoneId) {
		this.currentWeather = currentWeather;
		this.zoneId = zoneId;
		ZonedDateTime weatherTime = Utils.ofEpochSecond(currentWeather.getDt(), zoneId);
		ZonedDateTime sunriseTime = Utils.ofEpochSecond(currentWeather.getSunrise(), zoneId);
		ZonedDateTime sunsetTime = Utils.ofEpochSecond(currentWeather.getSunset(), zoneId);
		dateLabel.setText(DATE.format(weatherTime));
		timeLabel.setText(ZONED_TIME.format(weatherTime));
		Icon icon = Icons.getIcon(currentWeather.getClouds(), currentWeather.getWeather(), 50, 50);
		weatherIconLabel.setIcon(icon);
		weatherDescLabel.setText(Utils.getWeatherDesc(currentWeather.getWeather()));
		tempLabel.setText(String.format("%.1f", currentWeather.getTemp())+"˚C");
		apparentLabel.setText(String.format("%.1f", currentWeather.getFeelsLike())+"˚C");
		precipitationLabel.setText(String.format("%.2f", currentWeather.getRain().get1h())+"mm");
		humidityLabel.setText(currentWeather.getHumidity()+"%");
		windSpeedLabel.setText(Utils.getWindDesc(currentWeather.getWindSpeed()));
		windGustLabel.setText(Utils.getWindDesc(currentWeather.getWindGust()));
		windDirLabel.setText(Utils.getDirectionDesc(currentWeather.getWindDeg()));
		double uvi = currentWeather.getUvi();
		if(uvi < 3) {
			uviLabel.setBackground(Color.GREEN);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 6) {
			uviLabel.setBackground(Color.YELLOW);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 8) {
			uviLabel.setBackground(Color.ORANGE);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 11) {
			uviLabel.setBackground(Color.RED);
			uviLabel.setForeground(Color.WHITE);
		}
		else {
			uviLabel.setBackground(Color.MAGENTA);
			uviLabel.setForeground(Color.WHITE);
		}
		uviLabel.setText(String.format("%.1f", uvi));
		sunriseLabel.setText(TIME.format(sunriseTime));
		sunsetLabel.setText(TIME.format(sunsetTime));
	}
}
