package tw.edu.ntu.esoe.wetu.web.data.weather;

import java.util.Collections;
import java.util.List;

public class DailyWeather {

	private long dt;
	private long sunrise;
	private long sunset;
	private long moonrise;
	private long moonset;
	private double moonPhase;
	private DailyTemperature temp = new DailyTemperature();
	private DailyFeelsLike feelsLike = new DailyFeelsLike();
	private int pressure;
	private int humidity;
	private double dewPoint;
	private double windSpeed;
	private double windGust;
	private int windDeg;
	private int clouds;
	private double uvi;
	private double pop;
	private double rain;
	private double snow;
	private List<WeatherCondition> weather = Collections.emptyList();

	public long getDt() {
		return dt;
	}

	public long getSunrise() {
		return sunrise;
	}

	public long getSunset() {
		return sunset;
	}

	public long getMoonrise() {
		return moonrise;
	}

	public long getMoonset() {
		return moonset;
	}

	public double getMoonPhase() {
		return moonPhase;
	}

	public DailyTemperature getTemp() {
		return temp;
	}

	public DailyFeelsLike getFeelsLike() {
		return feelsLike;
	}

	public int getPressure() {
		return pressure;
	}

	public int getHumidity() {
		return humidity;
	}

	public double getDewPoint() {
		return dewPoint;
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public double getWindGust() {
		return windGust;
	}

	public int getWindDeg() {
		return windDeg;
	}

	public int getClouds() {
		return clouds;
	}

	public double getUvi() {
		return uvi;
	}

	public double getPop() {
		return pop;
	}

	public double getRain() {
		return rain;
	}

	public double getSnow() {
		return snow;
	}

	public List<WeatherCondition> getWeather() {
		return weather;
	}

	@Override
	public String toString() {
		return "DailyWeather [dt=" + dt + ", sunrise=" + sunrise + ", sunset=" + sunset + ", moonrise=" + moonrise
				+ ", moonset=" + moonset + ", moonPhase=" + moonPhase + ", temp=" + temp + ", feelsLike=" + feelsLike
				+ ", pressure=" + pressure + ", humidity=" + humidity + ", dewPoint=" + dewPoint + ", windSpeed="
				+ windSpeed + ", windGust=" + windGust + ", windDeg=" + windDeg + ", clouds=" + clouds + ", uvi=" + uvi
				+ ", pop=" + pop + ", rain=" + rain + ", snow=" + snow + ", weather=" + weather + "]";
	}
}
