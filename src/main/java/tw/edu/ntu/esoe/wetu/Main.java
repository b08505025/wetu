package tw.edu.ntu.esoe.wetu;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.swing.SwingUtilities;

import tw.edu.ntu.esoe.wetu.gui.WetuWindow;
import tw.edu.ntu.esoe.wetu.web.SslUtils;

public class Main {

	public static void main(String[] args) {
		try {
			SslUtils.setDefaultSSLSocketFactory();
		}
		catch(IOException | GeneralSecurityException e) {
			throw new RuntimeException("Unable to set SSL socket factory.", e);
		}
		SwingUtilities.invokeLater(()->{
			WetuWindow.createAndShowGui();
		});
	}
}
