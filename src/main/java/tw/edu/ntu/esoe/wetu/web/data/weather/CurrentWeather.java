package tw.edu.ntu.esoe.wetu.web.data.weather;

import java.util.Collections;
import java.util.List;

public class CurrentWeather {

	private long dt;
	private long sunrise;
	private long sunset;
	private double temp;
	private double feelsLike;
	private int pressure;
	private int humidity;
	private double dewPoint;
	private double uvi;
	private int clouds;
	private int visibility;
	private double windSpeed;
	private double windGust;
	private int windDeg;
	private HourlyPrecipitation rain = new HourlyPrecipitation();
	private HourlyPrecipitation snow = new HourlyPrecipitation();
	private List<WeatherCondition> weather = Collections.emptyList();

	public long getDt() {
		return dt;
	}

	public long getSunrise() {
		return sunrise;
	}

	public long getSunset() {
		return sunset;
	}

	public double getTemp() {
		return temp;
	}

	public double getFeelsLike() {
		return feelsLike;
	}

	public int getPressure() {
		return pressure;
	}

	public int getHumidity() {
		return humidity;
	}

	public double getDewPoint() {
		return dewPoint;
	}

	public double getUvi() {
		return uvi;
	}

	public int getClouds() {
		return clouds;
	}

	public int getVisibility() {
		return visibility;
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public double getWindGust() {
		return windGust;
	}

	public int getWindDeg() {
		return windDeg;
	}

	public HourlyPrecipitation getRain() {
		return rain;
	}

	public HourlyPrecipitation getSnow() {
		return snow;
	}

	public List<WeatherCondition> getWeather() {
		return weather;
	}

	@Override
	public String toString() {
		return "CurrentWeather [dt=" + dt + ", sunrise=" + sunrise + ", sunset=" + sunset + ", temp=" + temp
				+ ", feelsLike=" + feelsLike + ", pressure=" + pressure + ", humidity=" + humidity + ", dewPoint="
				+ dewPoint + ", uvi=" + uvi + ", clouds=" + clouds + ", visibility=" + visibility + ", windSpeed="
				+ windSpeed + ", windGust=" + windGust + ", windDeg=" + windDeg + ", rain=" + rain + ", snow=" + snow
				+ ", weather=" + weather + "]";
	}
}
