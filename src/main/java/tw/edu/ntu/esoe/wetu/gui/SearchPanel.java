package tw.edu.ntu.esoe.wetu.gui;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import tw.edu.ntu.esoe.wetu.userdata.Settings;
import tw.edu.ntu.esoe.wetu.web.data.geocoding.GeocodingResult;
import tw.edu.ntu.esoe.wetu.web.data.geocoding.NominatimRequest;

public class SearchPanel extends JPanel {

	public static final Settings SETTINGS = WetuWindow.SETTINGS;

	public final WetuWindow window;
	public final WeatherPanel weather;

	private final JButton backButton;
	private final JTextField searchField;
	private final JLabel searchLabel;
	private final JTextField langField;
	private final JLabel langLabel;
	private final JButton searchButton;
	private final JList<String> resultList;
	private final JScrollPane resultScrollPane;
	private final JButton confirmButton;
	private final JLabel poweredLabel;

	private List<GeocodingResult> geocodingResults = Collections.emptyList();

	SearchPanel(WetuWindow window, WeatherPanel weather) {
		super();
		this.window = window;
		this.weather = weather;

		setLayout(null);

		backButton = new JButton("返回");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.closePane();
			}
		});
		add(backButton);

		searchField = new JTextField();
		searchField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					searchButton.doClick();
				}
			}
		});
		add(searchField);

		searchLabel = new JLabel("搜尋");
		add(searchLabel);

		langField = new JTextField();
		langField.setText("zh-Hant");
		langField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					searchButton.doClick();
				}
			}
		});
		add(langField);

		langLabel = new JLabel("語言");
		add(langLabel);

		searchButton = new JButton("搜尋");
		searchButton.setMargin(new Insets(0, 0, 0, 0));
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchLocation();
			}
		});
		add(searchButton);

		resultList = new JList<>();
		resultList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		resultList.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					confirmButton.doClick();
				}
			}
		});
		resultList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					confirmButton.doClick();
				}
			}
		});

		resultScrollPane = new JScrollPane(resultList);
		add(resultScrollPane);

		confirmButton = new JButton("確認");
		confirmButton.setMargin(new Insets(0, 0, 0, 0));
		confirmButton.setEnabled(false);
		confirmButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SETTINGS.setCurrentLocation(geocodingResults.get(resultList.getSelectedIndex()));
				weather.updateWeather();
				window.closePane();
			}
		});
		add(confirmButton);

		poweredLabel = new JLabel("Powered by Nominatim");
		poweredLabel.setFont(getFont());
		poweredLabel.setForeground(Color.GRAY);
		poweredLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		add(poweredLabel);

		backButton.setBounds(0, 0, 150, 20);
	}

	@Override
	public void doLayout() {
		searchField.setBounds(40, 40, getWidth()-80-80-50, 20);
		searchLabel.setBounds(40, 20, getWidth()-80-80-50, 20);
		langField.setBounds(getWidth()-40-80-50, 40, 50, 20);
		langLabel.setBounds(getWidth()-40-80-50, 20, 50, 20);
		searchButton.setBounds(getWidth()-40-80, 40, 80, 20);
		resultScrollPane.setBounds(40, 60, getWidth()-80, getHeight()-60-60);
		confirmButton.setBounds(getWidth()/2-40, getHeight()-60, 80, 20);
		poweredLabel.setBounds(0, getHeight()-20, getWidth(), 20);
	}

	@Override
	public void addNotify() {
		super.addNotify();
		searchField.requestFocusInWindow();
	}

	private void searchLocation() {
		searchButton.setEnabled(false);
		confirmButton.setEnabled(false);
		resultList.setListData(new String[0]);
		String searchText = searchField.getText();
		String langText = langField.getText();
		new SwingWorker<List<GeocodingResult>, Void>() {
			@Override
			protected List<GeocodingResult> doInBackground() throws Exception {
				return NominatimRequest.searchLocation(searchText, langText);
			}
			@Override
			protected void done() {
				try {
					updateGeocodingResults(get());
				}
				catch(Exception e) {
					WetuWindow.showErrorMessage(e);
				}
				searchButton.setEnabled(true);
			}
		}.execute();
	}

	private void updateGeocodingResults(List<GeocodingResult> geocodingResults) {
		geocodingResults.removeIf(result->{
			String locClass = result.getLocClass();
			return !"boundary".equals(locClass) && !"natural".equals(locClass) && !"place".equals(locClass);
		});
		this.geocodingResults = geocodingResults;
		String[] arr = geocodingResults.stream().map(GeocodingResult::getDisplayName).toArray(String[]::new);
		resultList.setListData(arr);
		if(arr.length > 0) {
			resultList.setSelectedIndex(0);
			confirmButton.setEnabled(true);
		}
	}
}
