package tw.edu.ntu.esoe.wetu.gui;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import tw.edu.ntu.esoe.wetu.web.data.weather.WeatherCondition;

public class Utils {

	public static ZonedDateTime ofEpochSecond(long second, ZoneId zoneId) {
		return ZonedDateTime.ofInstant(Instant.ofEpochSecond(second), zoneId);
	}

	public static String getWeatherDesc(List<WeatherCondition> weather) {
		return weather.stream().map(WeatherCondition::getDescription).collect(Collectors.joining(", "));
	}

	public static String getDirectionDesc(int deg) {
		deg %= 360;
		if(deg <= 22) {
			return "偏北風";
		}
		else if(deg <= 67) {
			return "東北風";
		}
		else if(deg <= 112) {
			return "偏東風";
		}
		else if(deg <= 157) {
			return "東南風";
		}
		else if(deg <= 202) {
			return "偏南風";
		}
		else if(deg <= 202) {
			return "偏南風";
		}
		else if(deg <= 247) {
			return "西南風";
		}
		else if(deg <= 292) {
			return "偏西風";
		}
		else if(deg <= 337) {
			return "西北風";
		}
		else {
			return "偏北風";
		}
	}

	public static String getWindDesc(double speed) {
		if(speed < 1.6) {
			return "≤1級";
		}
		else if(speed < 3.4) {
			return "2級";
		}
		else if(speed < 5.5) {
			return "3級";
		}
		else if(speed < 8.0) {
			return "4級";
		}
		else if(speed < 10.8) {
			return "5級";
		}
		else if(speed < 13.9) {
			return "6級";
		}
		else if(speed < 17.2) {
			return "7級";
		}
		else if(speed < 20.8) {
			return "8級";
		}
		else if(speed < 24.5) {
			return "9級";
		}
		else if(speed < 28.5) {
			return "10級";
		}
		else if(speed < 32.7) {
			return "11級";
		}
		else if(speed < 37.0) {
			return "12級";
		}
		else if(speed < 41.5) {
			return "13級";
		}
		else if(speed < 46.2) {
			return "14級";
		}
		else if(speed < 51.0) {
			return "15級";
		}
		else if(speed < 56.1) {
			return "16級";
		}
		else if(speed < 61.3) {
			return "17級";
		}
		else {
			return ">17級";
		}
	}
}
