package tw.edu.ntu.esoe.wetu.web.data.geocoding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.GZIPInputStream;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import tw.edu.ntu.esoe.wetu.web.data.JsonUtils;

public class NominatimRequest {

	public static final Type GEOCODING_RESULT_LIST_TYPE = new TypeToken<List<GeocodingResult>>(){}.getType();
	private static long lastRequest = 0;

	/**
	 * Synchronized because Nominatim has a 1 request per second limit
	 * @param locationName The location name to search for.
	 * @param lang The language the search results should be in.
	 * @return The list of geocoding search results.
	 * @throws IOException
	 */
	public static synchronized List<GeocodingResult> searchLocation(String locationName, String lang) throws IOException {
		if(System.currentTimeMillis() - lastRequest < 1000) {
			try {
				Thread.sleep(1000 - (System.currentTimeMillis() - lastRequest));
			}
			catch(InterruptedException e) {
				throw new IOException(e);
			}
		}
		String apiUrl = "https://nominatim.openstreetmap.org/search?"
				+ "format=json"
				+ "&q=" + URLEncoder.encode(locationName, "UTF-8")
				+ "&accept-language=" + lang;
		System.out.println(apiUrl);
		
		URL url = new URL(apiUrl);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();

		connection.setRequestMethod("GET");
		connection.setUseCaches(false);
		connection.setInstanceFollowRedirects(false);

		connection.setRequestProperty("accept-encoding", "gzip");
		connection.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36");

		System.out.println(connection.getResponseCode()+" "+connection.getResponseMessage());
		System.out.println(connection.getHeaderFields());

		BufferedReader in;
		if("gzip".equals(connection.getContentEncoding())) {
			InputStreamReader reader = new InputStreamReader(new GZIPInputStream(connection.getInputStream()), StandardCharsets.UTF_8);
			in = new BufferedReader(reader);
		}
		else {
			InputStreamReader reader = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
			in = new BufferedReader(reader);
		}
		lastRequest = System.currentTimeMillis();
		JsonElement jsonElement = JsonParser.parseReader(in);
		return JsonUtils.GSON.fromJson(jsonElement, GEOCODING_RESULT_LIST_TYPE);	
	}
}
