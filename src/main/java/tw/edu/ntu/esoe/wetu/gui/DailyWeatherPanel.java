package tw.edu.ntu.esoe.wetu.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import tw.edu.ntu.esoe.wetu.userdata.Settings;
import tw.edu.ntu.esoe.wetu.web.data.weather.DailyWeather;

public class DailyWeatherPanel extends JPanel {

	public static final int PANEL_HEIGHT = 120;

	public static final Settings SETTINGS = WetuWindow.SETTINGS;

	public static final Color CELESTE = new Color(0xB2FFFF);

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("MM/dd");
	public static final DateTimeFormatter WEEKDAY = DateTimeFormatter.ofPattern("EEE");
	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm");

	private final JLabel dateLabel;
	private final JLabel weekdayLabel;
	private final JLabel weatherCatLabel;
	private final JLabel weatherIconLabel;
	private final JLabel weatherDescLabel;
	private final JLabel maxTempCatLabel;
	private final JLabel maxTempLabel;
	private final JLabel minTempCatLabel;
	private final JLabel minTempLabel;
	private final JLabel popCatLabel;
	private final JLabel popLabel;
	private final JLabel humidityCatLabel;
	private final JLabel humidityLabel;
	private final JLabel windSpeedCatLabel;
	private final JLabel windSpeedLabel;
	private final JLabel windGustCatLabel;
	private final JLabel windGustLabel;
	private final JLabel windDirCatLabel;
	private final JLabel windDirLabel;
	private final JLabel uviCatLabel;
	private final JLabel uviLabel;
	private final JLabel sunriseCatLabel;
	private final JLabel sunriseLabel;
	private final JLabel sunsetCatLabel;
	private final JLabel sunsetLabel;

	private final DailyWeather dailyWeather;
	private final ZoneId zoneId;

	DailyWeatherPanel(DailyWeather dailyWeather, ZoneId zoneId) {
		super();
		this.dailyWeather = dailyWeather;
		this.zoneId = zoneId;

		setLayout(null);
		setBackground(CELESTE);
		setBorder(BorderFactory.createEtchedBorder());
		setPreferredSize(new Dimension(getWidth(), PANEL_HEIGHT));

		dateLabel = new JLabel();
		dateLabel.setFont(getFont());
		dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(dateLabel);

		weekdayLabel = new JLabel();
		weekdayLabel.setFont(getFont());
		weekdayLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(weekdayLabel);

		weatherCatLabel = new JLabel("天氣狀況");
		weatherCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(weatherCatLabel);

		weatherIconLabel = new JLabel();
		weatherIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
		weatherIconLabel.setVerticalAlignment(SwingConstants.CENTER);
		add(weatherIconLabel);

		weatherDescLabel = new JLabel();
		weatherDescLabel.setFont(getFont());
		weatherDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(weatherDescLabel);

		maxTempCatLabel = new JLabel("最高溫度");
		maxTempCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(maxTempCatLabel);

		maxTempLabel = new JLabel();
		maxTempLabel.setFont(getFont().deriveFont(18F));
		maxTempLabel.setHorizontalAlignment(SwingConstants.CENTER);
		maxTempLabel.setVerticalAlignment(SwingConstants.CENTER);
		add(maxTempLabel);

		minTempCatLabel = new JLabel("最低溫度");
		minTempCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(minTempCatLabel);

		minTempLabel = new JLabel();
		minTempLabel.setFont(getFont().deriveFont(18F));
		minTempLabel.setHorizontalAlignment(SwingConstants.CENTER);
		minTempLabel.setVerticalAlignment(SwingConstants.CENTER);
		add(minTempLabel);

		popCatLabel = new JLabel("降雨機率");
		popCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(popCatLabel);

		popLabel = new JLabel();
		popLabel.setFont(getFont());
		popLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(popLabel);

		humidityCatLabel = new JLabel("相對溼度");
		humidityCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(humidityCatLabel);

		humidityLabel = new JLabel();
		humidityLabel.setFont(getFont());
		humidityLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(humidityLabel);

		windSpeedCatLabel = new JLabel("風力");
		windSpeedCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windSpeedCatLabel);

		windSpeedLabel = new JLabel();
		windSpeedLabel.setFont(getFont());
		windSpeedLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windSpeedLabel);

		windGustCatLabel = new JLabel("陣風");
		windGustCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windGustCatLabel);

		windGustLabel = new JLabel();
		windGustLabel.setFont(getFont());
		windGustLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windGustLabel);

		windDirCatLabel = new JLabel("風向");
		windDirCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windDirCatLabel);

		windDirLabel = new JLabel();
		windDirLabel.setFont(getFont());
		windDirLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(windDirLabel);

		uviCatLabel = new JLabel("最高紫外線");
		uviCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(uviCatLabel);

		uviLabel = new JLabel();
		uviLabel.setFont(getFont());
		uviLabel.setOpaque(true);
		uviLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(uviLabel);

		sunriseCatLabel = new JLabel("日出");
		sunriseCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(sunriseCatLabel);

		sunriseLabel = new JLabel();
		sunriseLabel.setFont(getFont());
		sunriseLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(sunriseLabel);

		sunsetCatLabel = new JLabel("日沒");
		sunsetCatLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(sunsetCatLabel);

		sunsetLabel = new JLabel();
		sunsetLabel.setFont(getFont());
		sunsetLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(sunsetLabel);

		setData();
	}

	@Override
	public void doLayout() {
		int s = getWidth();
		int s1 = s*1/9;
		int s2 = s*13/54;
		int s3 = s*10/27;
		int s4 = s*1/2;
		int s5 = s*17/27;
		int s6 = s*41/54;
		int s7 = s*8/9;
		dateLabel.setBounds(s1-40, 40, 80, 20);
		weekdayLabel.setBounds(s1-40, 60, 80, 20);
		weatherCatLabel.setBounds(s2-40, 15, 80, 20);
		weatherIconLabel.setBounds(s2-40, 35, 80, 50);
		weatherDescLabel.setBounds(s2-40, 85, 80, 20);
		maxTempCatLabel.setBounds(s3-60, 15, 120, 20);
		maxTempLabel.setBounds(s3-60, 35, 120, 25);
		minTempCatLabel.setBounds(s3-60, 60, 120, 20);
		minTempLabel.setBounds(s3-60, 80, 120, 25);
		popCatLabel.setBounds(s4-40, 20, 80, 20);
		popLabel.setBounds(s4-40, 40, 80, 20);
		humidityCatLabel.setBounds(s4-40, 60, 80, 20);
		humidityLabel.setBounds(s4-40, 80, 80, 20);
		windSpeedCatLabel.setBounds(s5-50, 20, 50, 20);
		windSpeedLabel.setBounds(s5-50, 40, 50, 20);
		windGustCatLabel.setBounds(s5, 20, 50, 20);
		windGustLabel.setBounds(s5, 40, 50, 20);
		windDirCatLabel.setBounds(s5-40, 60, 80, 20);
		windDirLabel.setBounds(s5-40, 80, 80, 20);
		uviCatLabel.setBounds(s6-60, 40, 120, 20);
		uviLabel.setBounds(s6-15, 60, 30, 20);
		sunriseCatLabel.setBounds(s7-40, 20, 80, 20);
		sunriseLabel.setBounds(s7-40, 40, 80, 20);
		sunsetCatLabel.setBounds(s7-40, 60, 80, 20);
		sunsetLabel.setBounds(s7-40, 80, 80, 20);
	}

	void setData() {
		ZonedDateTime weatherTime = Utils.ofEpochSecond(dailyWeather.getDt(), zoneId);
		ZonedDateTime sunriseTime = Utils.ofEpochSecond(dailyWeather.getSunrise(), zoneId);
		ZonedDateTime sunsetTime = Utils.ofEpochSecond(dailyWeather.getSunset(), zoneId);
		dateLabel.setText(DATE.format(weatherTime));
		weekdayLabel.setText(WEEKDAY.format(weatherTime));
		Icon icon = Icons.getIcon(dailyWeather.getClouds(), dailyWeather.getWeather(), 50, 50);
		weatherIconLabel.setIcon(icon);
		weatherDescLabel.setText(Utils.getWeatherDesc(dailyWeather.getWeather()));
		maxTempLabel.setText(String.format("%.1f", dailyWeather.getTemp().getMax())+"˚C");
		minTempLabel.setText(String.format("%.1f", dailyWeather.getTemp().getMin())+"˚C");
		popLabel.setText(Math.round(dailyWeather.getPop()*100)+"%");
		humidityLabel.setText(dailyWeather.getHumidity()+"%");
		windSpeedLabel.setText(Utils.getWindDesc(dailyWeather.getWindSpeed()));
		windGustLabel.setText(Utils.getWindDesc(dailyWeather.getWindGust()));
		windDirLabel.setText(Utils.getDirectionDesc(dailyWeather.getWindDeg()));
		double uvi = dailyWeather.getUvi();
		if(uvi < 3) {
			uviLabel.setBackground(Color.GREEN);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 6) {
			uviLabel.setBackground(Color.YELLOW);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 8) {
			uviLabel.setBackground(Color.ORANGE);
			uviLabel.setForeground(Color.BLACK);
		}
		else if(uvi < 11) {
			uviLabel.setBackground(Color.RED);
			uviLabel.setForeground(Color.WHITE);
		}
		else {
			uviLabel.setBackground(Color.MAGENTA);
			uviLabel.setForeground(Color.WHITE);
		}
		uviLabel.setText(String.format("%.1f", uvi));
		sunriseLabel.setText(TIME.format(sunriseTime));
		sunsetLabel.setText(TIME.format(sunsetTime));
	}
}
