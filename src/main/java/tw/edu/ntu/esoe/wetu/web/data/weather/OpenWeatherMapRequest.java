package tw.edu.ntu.esoe.wetu.web.data.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import tw.edu.ntu.esoe.wetu.web.data.JsonUtils;

public class OpenWeatherMapRequest {

	private static String openWeatherMapApiKey = "cb0856ff7ee7f51e38d4f9d3bb2c6a6c";

	/**
	 * 
	 * @param lat The latitude of the location.
	 * @param lon The longitude of the location.
	 * @param lang The language the data should be in.
	 * @return The forecast data.
	 * @throws IOException
	 */
	public static ForecastWeatherData getForecast(double lat, double lon, String lang) throws IOException {
		String apiUrl = "https://api.openweathermap.org/data/2.5/onecall?"
				+ "lat=" + lat
				+ "&lon=" + lon
				+ "&exclude=minutely,alerts"
				+ "&units=metric"
				+ "&lang=" + lang;
		JsonElement jsonElement = OpenWeatherMapRequest.getOpenWeatherMap(apiUrl);
		return JsonUtils.GSON.fromJson(jsonElement, ForecastWeatherData.class);
	}

	private static JsonElement getOpenWeatherMap(String apiUrl) throws IOException {
		System.out.println(apiUrl);
		apiUrl = apiUrl+"&appid="+openWeatherMapApiKey;

		URL url = new URL(apiUrl);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();

		connection.setRequestMethod("GET");
		connection.setUseCaches(false);
		connection.setInstanceFollowRedirects(false);

		connection.setRequestProperty("Accept-Encoding", "gzip");

		System.out.println(connection.getResponseCode()+" "+connection.getResponseMessage());
		System.out.println(connection.getHeaderFields());

		BufferedReader in;
		if("gzip".equals(connection.getContentEncoding())) {
			InputStreamReader reader = new InputStreamReader(new GZIPInputStream(connection.getInputStream()), StandardCharsets.UTF_8);
			in = new BufferedReader(reader);
		}
		else {
			InputStreamReader reader = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
			in = new BufferedReader(reader);
		}
		return JsonParser.parseReader(in);
	}
}
