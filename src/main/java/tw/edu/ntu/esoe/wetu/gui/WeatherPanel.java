package tw.edu.ntu.esoe.wetu.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.Timer;

import tw.edu.ntu.esoe.wetu.userdata.Settings;
import tw.edu.ntu.esoe.wetu.web.data.geocoding.GeocodingResult;
import tw.edu.ntu.esoe.wetu.web.data.weather.ForecastWeatherData;
import tw.edu.ntu.esoe.wetu.web.data.weather.OpenWeatherMapRequest;

public class WeatherPanel extends JPanel {

	public static final Settings SETTINGS = WetuWindow.SETTINGS;

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm:ss");

	/**
	 * 30 minutes
	 */
	public static final int UPDATE_INTERVAL = 30;

	public final WetuWindow window;
	public final SearchPanel search;
	public final CurrentWeatherPanel currentWeather;

	private final JButton searchButton;
	private final JLabel dateLabel;
	private final JLabel timeLabel;
	private final JLabel locationLabel;
	private final JButton refreshButton;
	private final JLabel nextUpdateLabel;
	private final JPanel hourlyWeatherPane;
	private final JPanel dailyWeatherPane;
	private final JScrollPane hourlyWeatherScrollPane;
	private final JScrollPane dailyWeatherScrollPane;
	private final JTabbedPane tabs;
	private final JLabel poweredLabel;

	private Instant lastUpdate = Instant.EPOCH;
	private boolean updating = false;

	private ForecastWeatherData forecastData;

	private Timer timer;

	WeatherPanel(WetuWindow window) {
		super();
		this.window = window;

		setLayout(null);

		search = new SearchPanel(window, this);
		currentWeather = new CurrentWeatherPanel();

		searchButton = new JButton("變更地點");
		searchButton.setMargin(new Insets(0, 0, 0, 0));
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.openPane(search);
			}
		});
		add(searchButton);

		LocalDateTime dateTime = LocalDateTime.now(ZoneId.systemDefault());
		String date = dateTime.format(DATE);
		String time = dateTime.format(TIME);

		dateLabel = new JLabel(date);
		dateLabel.setFont(getFont());
		dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(dateLabel);

		timeLabel = new JLabel(time);
		timeLabel.setFont(getFont());
		timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(timeLabel);

		locationLabel = new JLabel("請按 變更地點 設置地點");
		locationLabel.setFont(getFont());
		locationLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(locationLabel);

		refreshButton = new JButton("⟳");
		refreshButton.setMargin(new Insets(0, 0, 0, 0));
		refreshButton.setFont(getFont().deriveFont(Font.BOLD, 15F));
		refreshButton.setVerticalAlignment(SwingConstants.CENTER);
		refreshButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateWeather();
			}
		});
		add(refreshButton);

		nextUpdateLabel = new JLabel();
		nextUpdateLabel.setFont(getFont());
		nextUpdateLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		add(nextUpdateLabel);

		hourlyWeatherPane = new JPanel(new GridLayout(0, 1));

		dailyWeatherPane = new JPanel(new GridLayout(0, 1));

		hourlyWeatherScrollPane = new JScrollPane(hourlyWeatherPane);
		hourlyWeatherScrollPane.setHorizontalScrollBarPolicy(ScrollPaneLayout.HORIZONTAL_SCROLLBAR_NEVER);
		hourlyWeatherScrollPane.getVerticalScrollBar().setUnitIncrement(10);

		dailyWeatherScrollPane = new JScrollPane(dailyWeatherPane);
		dailyWeatherScrollPane.setHorizontalScrollBarPolicy(ScrollPaneLayout.HORIZONTAL_SCROLLBAR_NEVER);
		dailyWeatherScrollPane.getVerticalScrollBar().setUnitIncrement(10);

		tabs = new JTabbedPane();
		tabs.addTab("現在天氣", currentWeather);
		tabs.addTab("逐小時預報", hourlyWeatherScrollPane);
		tabs.addTab("一週預報", dailyWeatherScrollPane);
		add(tabs);

		poweredLabel = new JLabel("Powered by OpenWeatherMap");
		poweredLabel.setFont(getFont());
		poweredLabel.setForeground(Color.GRAY);
		poweredLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		add(poweredLabel);

		timer = new Timer(25, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(getParent() != null) {
					Instant now = Instant.now().truncatedTo(ChronoUnit.SECONDS);
					LocalDateTime dateTime = LocalDateTime.ofInstant(now, ZoneId.systemDefault());
					Duration duration = Duration.ofMinutes(UPDATE_INTERVAL).minus(Duration.between(lastUpdate, now));
					dateLabel.setText(DATE.format(dateTime));
					timeLabel.setText(TIME.format(dateTime));
					long seconds = duration.getSeconds();
					nextUpdateLabel.setText(String.format("%02d:%02d", seconds/60, seconds%60)+" 後更新");
					if(duration.isNegative()) {
						updateWeather();
					}
				}
			}
		});
		timer.start();

		updateWeather();
	}

	@Override
	public void doLayout() {
		searchButton.setBounds(getWidth()-150, 0, 150, 20);
		dateLabel.setBounds(0, 20, getWidth(), 20);
		timeLabel.setBounds(0, 40, getWidth(), 20);
		locationLabel.setBounds(0, 60, getWidth(), 20);
		refreshButton.setBounds(getWidth()-40, 100, 20, 20);
		nextUpdateLabel.setBounds(getWidth()-240, 100, 195, 20);
		tabs.setBounds(20, 100, getWidth()-40, getHeight()-120);
		poweredLabel.setBounds(0, getHeight()-20, getWidth(), 20);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
	}

	void updateWeather() {
		refreshButton.setEnabled(false);
		lastUpdate = Instant.now().truncatedTo(ChronoUnit.SECONDS);
		GeocodingResult location = SETTINGS.getCurrentLocation();
		if(location != null) {
			searchButton.setEnabled(false);
			locationLabel.setText(location.getDisplayName());
			new SwingWorker<ForecastWeatherData, Void>() {
				@Override
				protected ForecastWeatherData doInBackground() throws Exception {
					return OpenWeatherMapRequest.getForecast(location.getLat(), location.getLon(), "zh_tw");
				}
				@Override
				protected void done() {
					try {
						updateWeather(get());
					}
					catch(Exception e) {
						WetuWindow.showErrorMessage(e);
					}
				}
			}.execute();
		}
	}

	void updateWeather(ForecastWeatherData forecastData) {
		this.forecastData = forecastData;
		ZoneId zoneId = ZoneId.ofOffset("UTC", ZoneOffset.ofTotalSeconds(forecastData.getTimezoneOffset()));
		currentWeather.setData(forecastData.getCurrent(), zoneId);
		new SwingWorker<List<HourlyWeatherPanel>, Void>() {
			@Override
			protected List<HourlyWeatherPanel> doInBackground() throws Exception {
				return forecastData.getHourly().stream().map(w->new HourlyWeatherPanel(w, zoneId)).collect(Collectors.toList());
			}
			@Override
			protected void done() {
				hourlyWeatherPane.removeAll();
				try {
					get().forEach(hourlyWeatherPane::add);
				}
				catch(Exception e) {
					WetuWindow.showErrorMessage(e);
				}
			}
		}.execute();
		new SwingWorker<List<DailyWeatherPanel>, Void>() {
			@Override
			protected List<DailyWeatherPanel> doInBackground() throws Exception {
				return forecastData.getDaily().stream().map(w->new DailyWeatherPanel(w, zoneId)).collect(Collectors.toList());
			}
			@Override
			protected void done() {
				dailyWeatherPane.removeAll();
				try {
					get().forEach(dailyWeatherPane::add);
				}
				catch(Exception e) {
					WetuWindow.showErrorMessage(e);
				}
			}
		}.execute();
		searchButton.setEnabled(true);
		refreshButton.setEnabled(true);
	}
}
