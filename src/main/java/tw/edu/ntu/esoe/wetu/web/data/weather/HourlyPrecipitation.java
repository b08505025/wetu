package tw.edu.ntu.esoe.wetu.web.data.weather;

import com.google.gson.annotations.SerializedName;

public class HourlyPrecipitation {

	@SerializedName("1h")
	private double _1h = 0;

	public double get1h() {
		return _1h;
	}

	@Override
	public String toString() {
		return "Precipitation [1h=" + _1h + "]";
	}
}
