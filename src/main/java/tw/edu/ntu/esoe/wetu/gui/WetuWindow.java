package tw.edu.ntu.esoe.wetu.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import tw.edu.ntu.esoe.wetu.userdata.Settings;

public class WetuWindow extends JFrame {

	public static final int WINDOW_MIN_WIDTH = 600;
	public static final int WINDOW_MIN_HEIGHT = 500;

	public static final Settings SETTINGS = new Settings(new File("settings.json")).load();

	public static WetuWindow window;

	public static void createAndShowGui() {
		if(window != null) {
			return;
		}
		window = new WetuWindow();
		window.setVisible(true);
	}

	public final WeatherPanel weather;

	private WetuWindow() {
		super();

		setTitle("WetU");
		setMinimumSize(new Dimension(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT));
		setBounds(SETTINGS.getBounds());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLayout(new GridLayout(1, 1));
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				SETTINGS.setBounds(getBounds());
			}
			@Override
			public void componentMoved(ComponentEvent e) {
				SETTINGS.setBounds(getBounds());
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				SETTINGS.save();
			}
		});

		weather = new WeatherPanel(this);

		openPane(weather);
	}

	private LinkedList<Component> componentStack = new LinkedList<>();

	public void openPane(Component comp) {
		if(!componentStack.isEmpty()) {
			remove(componentStack.getLast());
		}
		componentStack.addLast(comp);
		add(comp);
		validate();
		repaint();
	}

	public void closePane() {
		if(componentStack.size() > 1) {
			remove(componentStack.pollLast());
			add(componentStack.getLast());
			validate();
			repaint();
		}
	}

	public static void showErrorMessage(Object e) {
		if(e instanceof Throwable) {
			((Throwable)e).printStackTrace();
		}
		JOptionPane.showMessageDialog(window, e, "Error", JOptionPane.ERROR_MESSAGE);
	}
}
