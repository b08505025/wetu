package tw.edu.ntu.esoe.wetu.web.data.geocoding;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

public class GeocodingResult {

	private int placeId;
	private String osmType = "";
	private int osmId;
	private double[] boundingbox = new double[4];
	private double lat;
	private double lon;
	private String displayName = "";
	@SerializedName("class")
	private String locClass = "";
	private String type = "";
	private double importance;
	private String icon = "";

	public int getPlaceId() {
		return placeId;
	}

	public String getOsmType() {
		return osmType;
	}

	public int getOsmId() {
		return osmId;
	}

	public double[] getBoundingbox() {
		return boundingbox;
	}

	public double getLat() {
		return lat;
	}

	public double getLon() {
		return lon;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getLocClass() {
		return locClass;
	}

	public String getType() {
		return type;
	}

	public double getImportance() {
		return importance;
	}

	public String getIcon() {
		return icon;
	}

	@Override
	public String toString() {
		return "GeocodingResult [placeId=" + placeId + ", osmType=" + osmType + ", osmId=" + osmId + ", boundingbox="
				+ Arrays.toString(boundingbox) + ", lat=" + lat + ", lon=" + lon + ", displayName=" + displayName
				+ ", locClass=" + locClass + ", type=" + type + ", importance=" + importance + ", icon=" + icon + "]";
	}
}
