package tw.edu.ntu.esoe.wetu.web.data.weather;

import java.util.Collections;
import java.util.List;

public class ForecastWeatherData {

	private double lat;
	private double lon;
	private String timezone = "";
	private int timezoneOffset;
	private CurrentWeather current = new CurrentWeather();
	private List<HourlyWeather> hourly = Collections.emptyList();
	private List<DailyWeather> daily = Collections.emptyList();

	public double getLat() {
		return lat;
	}

	public double getLon() {
		return lon;
	}

	public String getTimezone() {
		return timezone;
	}

	public int getTimezoneOffset() {
		return timezoneOffset;
	}

	public CurrentWeather getCurrent() {
		return current;
	}

	public List<HourlyWeather> getHourly() {
		return hourly;
	}

	public List<DailyWeather> getDaily() {
		return daily;
	}

	@Override
	public String toString() {
		return "ForecastWeatherData [lat=" + lat + ", lon=" + lon + ", timezone=" + timezone + ", timezoneOffset="
				+ timezoneOffset + ", current=" + current + ", hourly=" + hourly + ", daily=" + daily + "]";
	}
}
