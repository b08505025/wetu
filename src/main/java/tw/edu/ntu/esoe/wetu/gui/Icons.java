package tw.edu.ntu.esoe.wetu.gui;

import java.awt.Dimension;
import java.net.URI;
import java.util.List;

import javax.swing.Icon;

import com.kitfox.svg.app.beans.SVGIcon;

import tw.edu.ntu.esoe.wetu.Main;
import tw.edu.ntu.esoe.wetu.web.data.weather.WeatherCondition;

public class Icons {

	public static Icon getIcon(int clouds, List<WeatherCondition> weather, int width, int height) {
		boolean fog = weather.stream().anyMatch(w->isAtmosphere(w.getId()));
		WeatherCondition condition = weather.stream().filter(w->!isAtmosphere(w.getId())).findFirst().orElse(weather.get(0));
		boolean night = condition.getIcon().contains("n");
		int id = condition.getId();
		String number = null;
		switch(id/100) {
		case 2:
			if(clouds <= 50) {
				number = !fog ? "15" : "35";
			}
			else {
				number = !fog ? "18" : "41";
			}
			break;
		case 3:
		case 5:
			if(id == 511) {
				number = !fog ? "23" : "37";
			}
			else {
				if(clouds <= 50) {
					number = !fog ? "08" : "38";
				}
				else {
					number = !fog ? "11" : "39";
				}
			}
			break;
		case 6:
			switch(id) {
			case 611: case 612: case 613:
			case 615: case 616:
				number = !fog ? "23" : "37";
				break;
			default:
				number = "42";
				break;
			}
			break;
		case 8:
			if(clouds <= 10) {
				number = !fog ? "01" : "24";
			}
			else if(clouds <= 50) {
				number = !fog ? "04" : "27";
			}
			else {
				number = !fog ? "07" : "28";
			}
			break;
		}
		if(number == null) {
			return null;
		}
		return getIcon((!night ? "day_" : "night_")+number+".svg", width, height);
	}

	public static Icon getIcon(String path, int width, int height) {
		try {
			URI uri = Main.class.getClassLoader().getResource(path).toURI();
			SVGIcon icon = new SVGIcon();
			icon.setSvgURI(uri);
			//return new ImageIcon(icon.getImage().getScaledInstance(width, height, Image.SCALE_FAST));
			icon.setAutosize(SVGIcon.AUTOSIZE_BESTFIT);
			icon.setPreferredSize(new Dimension(width, height));
			return icon;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean isAtmosphere(int id) {
		return id / 100 == 7;
	}
}
