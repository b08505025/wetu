package tw.edu.ntu.esoe.wetu.web.data;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtils {

	/**
	 * The Gson to use when serializing and deserializing JSON.
	 */
	public static final Gson GSON = new GsonBuilder().
			setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).
			serializeNulls().
			setLenient().
			setPrettyPrinting().
			create();
}
