package tw.edu.ntu.esoe.wetu.web.data.weather;

public class DailyFeelsLike {

	private double morn;
	private double day;
	private double eve;
	private double night;

	public double getMorn() {
		return morn;
	}

	public double getDay() {
		return day;
	}

	public double getEve() {
		return eve;
	}

	public double getNight() {
		return night;
	}

	@Override
	public String toString() {
		return "DailyFeelsLike [morn=" + morn + ", day=" + day + ", eve=" + eve + ", night=" + night + "]";
	}
}
