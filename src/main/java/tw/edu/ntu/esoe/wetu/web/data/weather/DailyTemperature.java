package tw.edu.ntu.esoe.wetu.web.data.weather;

public class DailyTemperature {

	private double morn;
	private double day;
	private double eve;
	private double night;
	private double min;
	private double max;

	public double getMorn() {
		return morn;
	}

	public double getDay() {
		return day;
	}

	public double getEve() {
		return eve;
	}

	public double getNight() {
		return night;
	}

	public double getMin() {
		return min;
	}

	public double getMax() {
		return max;
	}

	@Override
	public String toString() {
		return "DailyTemperature [morn=" + morn + ", day=" + day + ", eve=" + eve + ", night=" + night + ", min=" + min
				+ ", max=" + max + "]";
	}
}
